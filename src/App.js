import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./screens/Home";
import Login from "./screens/Login";
import Register from "./screens/Register";
import AuthState from "./context/auth/AuthState";
import Navbar from "./components/Navbar";
import Appointments from "./screens/Appointments";
import Admin from "./screens/Admin";

function App() {
  return (
    <>
      <AuthState>
        <Router>
          <Navbar />
          <div>
            <Switch>
              <Route path="/appointments" exact component={Appointments} />
              <Route path="/register" exact component={Register} />
              <Route path="/login" exact component={Login} />
              <Route path="/" exact component={Home} />
              <Route path="/admin" exact component={Admin} />
            </Switch>
          </div>
        </Router>
      </AuthState>
    </>
  );
}

export default App;
