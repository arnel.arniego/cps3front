import React, { useState, useEffect, useContext } from "react";
import AuthContext from "../context/auth/authContext";
import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import moment from "moment";

const Appointments = props => {
  const authContext = useContext(AuthContext);
  const { isAuthenticated, user } = authContext;

  const [bookings, setBookings] = useState([]);
  useEffect(() => {
    if (!isAuthenticated) {
      props.history.push("/");
    }

    const token = localStorage.getItem("jwtToken");
    setAuthToken(token);

    axios
      .get("/api/bookings/getbookings")
      .then(res => {
        setBookings(res.data);
      }) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  }, []);

  const cancelBooking = id => {
    axios
      .put("/api/bookings/cancelbooking", { id })
      .then(res => {
        let newbookings = bookings.map(booking => {
          if (booking._id === res.data._id) {
            booking.status = "cancelled";
          }
          return booking;
        });
        setBookings(newbookings);
      }) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  };

  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  const editBooking = row => {
    setState({ ...row });
    toggle();
  };

  const cancelFormatter = (cell, row) => {
    return (
      <>
        <button
          className="btn-danger"
          style={{ padding: "5px" }}
          onClick={() => {
            cancelBooking(cell);
          }}
        >
          Cancel
        </button>
        <button
          className="btn-warning"
          style={{ padding: "5px" }}
          onClick={() => {
            editBooking(row);
          }}
        >
          Edit
        </button>
      </>
    );
  };

  const columns = [
    {
      dataField: "datetime",
      text: "Date & Time"
    },
    {
      dataField: "service",
      text: "Service"
    },
    {
      dataField: "stylist",
      text: "Stylist"
    },
    {
      dataField: "address",
      text: "Address"
    },
    {
      dataField: "status",
      text: "Status"
    },
    {
      dataField: "_id",
      text: "Action",
      formatter: cancelFormatter
    }
  ];

  const [state, setState] = useState({
    _id: "",
    datetime: new Date(),
    service: "",
    stylist: "",
    address: ""
  });

  const dateChange = value => {
    setState({ ...state, ["datetime"]: value });
  };

  const onChange = e => {
    setState({ ...state, [e.target.id]: e.target.value });
  };

  const onSubmit = e => {
    e.preventDefault();

    if (!isAuthenticated) {
      props.history.push("/login");
    }

    const booking = {
      id: state._id,
      datetime: moment(state.datetime).format("MM/DD/YYYY HH:mm A"),
      service: state.service,
      stylist: state.stylist,
      address: state.address
    };

    axios
      .post("/api/bookings/updatebooking", booking)
      .then(res => {
        toggle();
        axios
          .get("/api/bookings/getbookings")
          .then(res => {
            setBookings(res.data);
          }) // re-direct to login on successful register
          .catch(err => {
            console.log(err.message);
          });
      }) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  };

  return (
    <>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
          <form onSubmit={onSubmit}>
            <div class="form-group">
              <label>Date & Time</label>
              <Datetime value={state.datetime} onChange={dateChange} />
            </div>
            <div class="form-group">
              <label>Service</label>
              <select
                id="service"
                class="form-control"
                value={state.service}
                onChange={onChange}
                defaultValue=""
              >
                <option value="">Choose service...</option>
                <option value="Makeup">Makeup</option>
                <option value="Hair">Hair</option>
                <option value="Nails">Nails</option>
                <option value="Events">Events</option>
              </select>
            </div>
            <div class="form-group">
              <label>Stylist</label>
              <select
                id="stylist"
                class="form-control"
                value={state.stylist}
                onChange={onChange}
                defaultValue=""
              >
                <option value="">Choose stylist...</option>
                <option value="Jennifer Lawrence">Jennifer Lawrence</option>
                <option value="Scarlett Johansson">Scarlett Johansson</option>
                <option value="Natalie Portman">Natalie Portman</option>
                <option value="Emma Stone">Emma Stone</option>
                <option value="Gal Gadot">Gal Gadot</option>
                <option value="Alexandra Daddario">Alexandra Daddario</option>
                <option value="Margot Robbie">Margot Robbie</option>
                <option value="sMegan Fox">Megan Fox</option>
                <option value="Jessica Alba">Jessica Alba</option>
                <option value="Léa Seydoux">Léa Seydoux</option>
                <option value="Rachel McAdams">Rachel McAdams</option>
                <option value="Mila Kunis">Mila Kunis</option>
              </select>
            </div>
            <div class="form-group">
              <label>Complete Address</label>
              <textarea
                class="form-control"
                id="address"
                rows="3"
                onChange={onChange}
                value={state.address}
              ></textarea>
            </div>
            <button type="submit" className="btn btn-primary btn-block">
              Update
            </button>
          </form>
        </ModalBody>
      </Modal>
      <h2 className="text-center mb-3">My Appointments</h2>
      {bookings.length !== 0 ? (
        <>
          <div className="container">
            <BootstrapTable
              bootstrap4
              keyField="_id"
              data={bookings}
              columns={columns}
              pagination={paginationFactory()}
            />
          </div>
        </>
      ) : (
        <>
          <p className="text-center">No Appoinments</p>
        </>
      )}
      <footer id="footer">
        <div className="container">
          <div className="copyright">
            &copy; Copyright <strong>Nhel</strong>. All Rights Reserved 2020
          </div>
          <div className="credits">
            {" "}
            <a href="">Nhel</a>
          </div>
        </div>
      </footer>
      <a href="#" className="back-to-top">
        <i className="fa fa-chevron-up"></i>
      </a>
    </>
  );
};

export default Appointments;
