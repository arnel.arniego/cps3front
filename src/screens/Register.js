import React, { useContext, useState, useEffect } from "react";
import AuthContext from "../context/auth/authContext";

import "../css/main.css";
import Navbar from "../components/Navbar";

const Register = props => {
  const authContext = useContext(AuthContext);
  const { registerUser, errors, isAuthenticated } = authContext;

  const [state, setState] = useState({
    firstname: "",
    lastname: "",
    email: "",
    mobileno: "",
    password: "",
    confirmpassword: "",
    errors: {}
  });

  const onChange = e => {
    setState({ ...state, [e.target.id]: e.target.value });
  };

  const onSubmit = e => {
    e.preventDefault();

    const newUser = {
      firstname: state.firstname,
      lastname: state.lastname,
      email: state.email,
      mobileno: state.mobileno,
      password: state.password,
      confirmpassword: state.confirmpassword
    };

    registerUser(newUser, props.history);
  };

  useEffect(() => {
    if (isAuthenticated) {
      props.history.push("/");
    }

    if (errors) {
      setState({ ...state, errors: errors });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [errors, isAuthenticated, props.history]);

  return (
    <>
      <div className="page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins">
        <div className="wrapper wrapper--w780">
          <div className="card card-3">
            <div className="card-heading"></div>
            <div className="card-body">
              <h2 className="title">Sign Up</h2>
              <form onSubmit={onSubmit}>
                <div className="input-group">
                  <input
                    className="input--style-3"
                    type="name"
                    placeholder="First Name"
                    name="name"
                    id="firstname"
                    value={state.firstname}
                    onChange={onChange}
                  />
                </div>

                <div className="input-group">
                  <input
                    className="input--style-3"
                    type="name"
                    placeholder="Last Name"
                    name="name"
                    id="lastname"
                    value={state.lastname}
                    onChange={onChange}
                  />
                </div>

                <div className="input-group">
                  <input
                    className="input--style-3"
                    type="no."
                    placeholder="Mobile Number"
                    name="no."
                    id="mobileno"
                    value={state.mobileno}
                    onChange={onChange}
                  />
                </div>

                <div className="input-group">
                  <input
                    className="input--style-3"
                    type="email"
                    placeholder="Email"
                    name="email"
                    id="email"
                    value={state.email}
                    onChange={onChange}
                  />
                </div>

                <div className="input-group">
                  <input
                    className="input--style-3"
                    type="password"
                    placeholder="Password"
                    name="password"
                    id="password"
                    value={state.password}
                    onChange={onChange}
                  />
                </div>

                <div className="input-group">
                  <input
                    className="input--style-3"
                    type="password"
                    placeholder="Confirm Password"
                    name="password"
                    id="confirmpassword"
                    value={state.confirmpassword}
                    onChange={onChange}
                  />
                </div>

                <div className="p-t-10">
                  <button className="btn btn--pill btn--green" type="submit">
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <footer id="footer">
        <div className="container">
          <div className="copyright">
            &copy; Copyright <strong>Nhel</strong>. All Rights Reserved 2020
          </div>
          <div className="credits">
            {" "}
            <a href="">Nhel</a>
          </div>
        </div>
      </footer>
      <a href="#" className="back-to-top">
        <i className="fa fa-chevron-up"></i>
      </a>
    </>
  );
};
export default Register;
