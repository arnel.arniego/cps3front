import React, { useState, useEffect, useContext } from "react";
import AuthContext from "../context/auth/authContext";
import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import moment from "moment";

const Admin = props => {
  const authContext = useContext(AuthContext);
  const { isAuthenticated, user } = authContext;

  const [bookings, setBookings] = useState([]);
  useEffect(() => {
    if (!isAuthenticated) {
      props.history.push("/");
    }

    const token = localStorage.getItem("jwtToken");
    setAuthToken(token);

    axios
      .get("/api/bookings/allbookings")
      .then(res => {
        setBookings(res.data);
      }) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  }, []);

  const approveBooking = id => {
    axios
      .put("/api/bookings/approvebooking", { id })
      .then(res => {
        let newbookings = bookings.map(booking => {
          if (booking._id === res.data._id) {
            booking.status = "approved";
          }
          return booking;
        });
        setBookings(newbookings);
      }) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  };

  const rejectBooking = id => {
    axios
      .put("/api/bookings/rejectbooking", { id })
      .then(res => {
        let newbookings = bookings.map(booking => {
          if (booking._id === res.data._id) {
            booking.status = "rejected";
          }
          return booking;
        });
        setBookings(newbookings);
      }) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  };

  const cancelFormatter = (cell, row) => {
    return (
      <>
        <button
          className="btn-success"
          style={{ padding: "5px" }}
          onClick={() => {
            approveBooking(cell);
          }}
        >
          Approve
        </button>
        <button
          className="btn-danger"
          style={{ padding: "5px" }}
          onClick={() => {
            rejectBooking(cell);
          }}
        >
          Reject
        </button>
      </>
    );
  };

  const columns = [
    {
      dataField: "user.email",
      text: "email"
    },
    {
      dataField: "datetime",
      text: "Date & Time"
    },
    {
      dataField: "service",
      text: "Service"
    },
    {
      dataField: "stylist",
      text: "Stylist"
    },
    {
      dataField: "address",
      text: "Address"
    },
    {
      dataField: "status",
      text: "Status"
    },
    {
      dataField: "_id",
      text: "Action",
      formatter: cancelFormatter
    }
  ];

  return (
    <>
      <h2 className="text-center mb-3">My Appointments</h2>
      {bookings.length !== 0 ? (
        <>
          <div className="container">
            <BootstrapTable
              bootstrap4
              keyField="_id"
              data={bookings}
              columns={columns}
              pagination={paginationFactory()}
            />
          </div>
        </>
      ) : (
        <>
          <p className="text-center">No Appoinments</p>
        </>
      )}
      <footer id="footer">
        <div className="container">
          <div className="copyright">
            &copy; Copyright <strong>Nhel</strong>. All Rights Reserved 2020
          </div>
          <div className="credits">
            {" "}
            <a href="">Nhel</a>
          </div>
        </div>
      </footer>
      <a href="#" className="back-to-top">
        <i className="fa fa-chevron-up"></i>
      </a>
    </>
  );
};

export default Admin;
