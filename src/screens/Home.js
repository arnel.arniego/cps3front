import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";

// import "bootstrap/dist/css/bootstrap.min.css";
// import $ from "jquery";
// import Popper from "popper.js";
// import "bootstrap/dist/js/bootstrap.bundle.min";
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import moment from "moment";
import "../lib/bootstrap/css/bootstrap.min.css";
import "../lib/font-awesome/css/font-awesome.min.css";
import "../lib/animate/animate.min.css";
import "../lib/ionicons/css/ionicons.min.css";
import "../lib/owlcarousel/assets/owl.carousel.min.css";
import "../lib/magnific-popup/magnific-popup.css";
import "../lib/ionicons/css/ionicons.min.css";
import "../css/style.css";
import axios from "axios";
import AuthContext from "../context/auth/authContext";

const Home = props => {
  const authContext = useContext(AuthContext);
  const { isAuthenticated } = authContext;

  const [state, setState] = useState({
    datetime: new Date(),
    service: "",
    stylist: "",
    address: ""
  });

  const dateChange = value => {
    setState({ ...state, ["datetime"]: value });
  };

  const onChange = e => {
    setState({ ...state, [e.target.id]: e.target.value });
  };

  const onSubmit = e => {
    e.preventDefault();

    if (!isAuthenticated) {
      props.history.push("/login");
    }

    const booking = {
      datetime: moment(state.datetime).format("MM/DD/YYYY HH:mm A"),
      service: state.service,
      stylist: state.stylist,
      address: state.address
    };

    axios
      .post("/api/bookings/newbooking", booking)
      .then(res => props.history.push("/appointments")) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  };

  return (
    <>
      <section id="intro">
        <div className="intro-content">
          <h2>
            <span>Essentials & Inovations</span>
            <br />
            Best Squad in City
            <br />
          </h2>

          <h1>
            <span> We bring beauty to you.</span>
            <br />
          </h1>

          <h5>
            <span>In-home hair, makeup and nail services.</span>
            <br />
            Must-have products. Anytime, anywhere.
            <br />
            <br />
          </h5>
          <div>
            <a href="#services" className="btn-get-started scrollto">
              OFFERS
            </a>
          </div>
        </div>
        <div
          className="item"
          style={{ backgroundImage: `url('img/intro-carousel/1.jpg')` }}
        ></div>
      </section>
      <main id="main">
        <section id="about" className="wow fadeInUp sec-padding">
          <div className="container">
            <div className="section-header">
              <h2>N - Squad</h2>
              <p>
                Making your beauty appointments just got easier. Click 'Book an
                Appointment' to choose from one of our iconic Hair and Makeup
                looks or luxury nail services. First time user? N - Squad-prizes
                await, sign up today.
              </p>
            </div>
            <div className="row">
              <div className="col-lg-6 about-img">
                <img src="img/about-img.png" alt="" />
              </div>

              <div className="col-lg-6 content">
                <h2>A VIRTUAL AGENCY</h2>
                <h3>
                  We also act as a virtual agency connecting stylists with
                  clients. Stylists act as freelance artists; making their own
                  hours, building clientele, and earning creative opportunities.
                </h3>
                <p>
                  Our menu is a collection of signature looks from the hottest
                  beauty trends in town. Hand-selected by Nhel himself, just for
                  you.
                </p>
                <ul>
                  <li>
                    <i class="icon ion-ios-checkmark-outline"></i> From the
                    essential blow-dry to effortless curls, choose a chic style
                    for any occasion.
                  </li>
                  <li>
                    <i class="icon ion-ios-checkmark-outline"></i> From a clean
                    natural look to sleek evening glamour, we’ve got you
                    covered.
                  </li>
                  <li>
                    <i class="icon ion-ios-checkmark-outline"></i> From a
                    perfect pout to a full face, you'll be red-carpet-ready
                    day-to-night.
                  </li>
                  <li>
                    <i class="icon ion-ios-checkmark-outline"></i> From a simple
                    polish change to a full mani/pedi, you can enjoy in the
                    comfort of your own home.
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
        <section id="services" className="sec-padding">
          <div className="container">
            <div className="section-header">
              <h2>Our Services</h2>
              <p>
                Life often gets so busy, but that should never stand in the way
                of looking and feeling your best. With a community of skilled
                artists ready to get you glam, all you have to do is book an
                appointment and they’ll arrive straight to your door. We work
                with your schedule.
              </p>
              <p>
                Glamsquad offers in-home beauty services by the top beauty
                professionals.
              </p>
            </div>

            <div className="row">
              <div className="col-lg-4">
                <div className="card wow fadeInLeft" style={{ width: "18rem" }}>
                  <img
                    src="img/services/1.jpg"
                    alt=""
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h5 className="card-title">HAIR</h5>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="card wow fadeInLeft" style={{ width: "18rem" }}>
                  <img
                    src="img/services/2.jpg"
                    alt=""
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h5 className="card-title">SKIN</h5>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="card wow fadeInLeft" style={{ width: "18rem" }}>
                  <img
                    src="img/services/3.jpg"
                    alt=""
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h5 className="card-title">MAKEUP</h5>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="card wow fadeInLeft" style={{ width: "18rem" }}>
                  <img
                    src="img/services/4.jpg"
                    alt=""
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h5 className="card-title">ESSENTIALS</h5>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="card wow fadeInLeft" style={{ width: "18rem" }}>
                  <img
                    src="img/services/5.jpg"
                    alt=""
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h5 className="card-title">INOVATIONS</h5>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="card wow fadeInLeft" style={{ width: "18rem" }}>
                  <img
                    src="img/services/6.jpg"
                    alt=""
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h5 className="card-title">BRIDAL</h5>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-4">
                <div className="box wow fadeInRight" data-wow-delay="0.2s">
                  <div className="icon">
                    <i className="fa fa-map"></i>
                  </div>
                  <h4 className="title">
                    <a href="">N - Squad comes to you</a>
                  </h4>
                  <p className="description">
                    Whether in your hone, office, or hotel; sit back and relax
                    as we bring the best service straight to your door.
                  </p>
                </div>
              </div>

              <div className="col-lg-4">
                <div className="box wow fadeInLeft" data-wow-delay="0.2s">
                  <div className="icon">
                    <i className="fa fa-shopping-bag"></i>
                  </div>
                  <h4 className="title">
                    <a href="">Works only with experts.</a>
                  </h4>
                  <p className="description">
                    Every beauty professional is put through rigorous testing
                    before being accepted into our network.
                  </p>
                </div>
              </div>

              <div className="col-lg-4">
                <div className="box wow fadeInRight" data-wow-delay="0.2s">
                  <div className="icon">
                    <i className="fa fa-map"></i>
                  </div>
                  <h4 className="title">
                    <a href="">Select your date & time.</a>
                  </h4>
                  <p className="description">
                    With the push of a button, we deliver beauty experts
                    straight to your door. No need to leave your house.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="booking" className="wow fadeInUp sec-padding">
          <div className="container">
            <div class="section-header">
              <h2>Appointment</h2>
              <p>
                Life often gets so busy, but that should never stand in the way
                of looking and feeling your best. With a community of skilled
                artists ready to get you glam, all you have to do is book an
                appointment and they’ll arrive straight to your door. We work
                with your schedule.
              </p>
            </div>

            <div className="row">
              <div className="col-lg-4 mx-auto">
                <form onSubmit={onSubmit}>
                  <div class="form-group">
                    <label>Date & Time</label>
                    <Datetime value={state.datetime} onChange={dateChange} />
                  </div>
                  <div class="form-group">
                    <label>Service</label>
                    <select
                      id="service"
                      class="form-control"
                      value={state.service}
                      onChange={onChange}
                      defaultValue=""
                    >
                      <option value="">Choose service...</option>
                      <option value="Makeup">Makeup</option>
                      <option value="Hair">Hair</option>
                      <option value="Nails">Nails</option>
                      <option value="Events">Events</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Stylist</label>
                    <select
                      id="stylist"
                      class="form-control"
                      value={state.stylist}
                      onChange={onChange}
                      defaultValue=""
                    >
                      <option value="">Choose stylist...</option>
                      <option value="Jennifer Lawrence">
                        Jennifer Lawrence
                      </option>
                      <option value="Scarlett Johansson">
                        Scarlett Johansson
                      </option>
                      <option value="Natalie Portman">Natalie Portman</option>
                      <option value="Emma Stone">Emma Stone</option>
                      <option value="Gal Gadot">Gal Gadot</option>
                      <option value="Alexandra Daddario">
                        Alexandra Daddario
                      </option>
                      <option value="Margot Robbie">Margot Robbie</option>
                      <option value="sMegan Fox">Megan Fox</option>
                      <option value="Jessica Alba">Jessica Alba</option>
                      <option value="Léa Seydoux">Léa Seydoux</option>
                      <option value="Rachel McAdams">Rachel McAdams</option>
                      <option value="Mila Kunis">Mila Kunis</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Complete Address</label>
                    <textarea
                      class="form-control"
                      id="address"
                      rows="3"
                      onChange={onChange}
                      value={state.address}
                    ></textarea>
                  </div>
                  <button type="submit" className="btn btn-primary btn-block">
                    Book Now
                  </button>
                </form>
              </div>
            </div>
          </div>
        </section>
        <section id="clients" className="wow fadeInUp sec-padding">
          <div className="container">
            <div className="section-header">
              <h2>Products</h2>
              <p>Created for you, inspired by you.</p>
              <p>Services and products to make you feel ready, every day.</p>
            </div>

            <div className="owl-carousel clients-carousel">
              <img src="img/clients/client-1.jpg" alt="" />
              <img src="img/clients/client-2.jpg" alt="" />
              <img src="img/clients/client-3.jpg" alt="" />
              <img src="img/clients/client-4.jpg" alt="" />
              <img src="img/clients/client-5.jpg" alt="" />
              <img src="img/clients/client-6.jpg" alt="" />
            </div>
          </div>
        </section>
        <section id="portfolio" className="wow fadeInUp sec-padding">
          <div className="container">
            <div className="section-header">
              <h2>GALLERY</h2>
              <p>
                Life often gets so busy, but that should never stand in the way
                of looking and feeling your best. With a community of skilled
                artists ready to get you glam, all you have to do is book an
                appointment and they’ll arrive straight to your door. We work
                with your schedule.
              </p>
            </div>
          </div>

          <div className="container-fluid">
            <div className="row no-gutters">
              <div className="col-lg-3 col-md-4">
                <div className="portfolio-item wow fadeInUp">
                  <a href="img/portfolio/1.jpg" className="portfolio-popup">
                    <img src="img/portfolio/1.jpg" alt="" />
                    <div className="portfolio-overlay">
                      <div className="portfolio-info">
                        <h2 className="wow fadeInUp"></h2>
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="portfolio-item wow fadeInUp">
                  <a href="img/portfolio/2.jpg" className="portfolio-popup">
                    <img src="img/portfolio/2.jpg" alt="" />
                    <div className="portfolio-overlay">
                      <div className="portfolio-info">
                        <h2 className="wow fadeInUp"></h2>
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="portfolio-item wow fadeInUp">
                  <a href="img/portfolio/3.jpg" className="portfolio-popup">
                    <img src="img/portfolio/3.jpg" alt="" />
                    <div className="portfolio-overlay">
                      <div className="portfolio-info">
                        <h2 className="wow fadeInUp"></h2>
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="portfolio-item wow fadeInUp">
                  <a href="img/portfolio/4.jpg" className="portfolio-popup">
                    <img src="img/portfolio/4.jpg" alt="" />
                    <div className="portfolio-overlay">
                      <div className="portfolio-info">
                        <h2 className="wow fadeInUp"></h2>
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="portfolio-item wow fadeInUp">
                  <a href="img/portfolio/5.jpg" className="portfolio-popup">
                    <img src="img/portfolio/5.jpg" alt="" />
                    <div className="portfolio-overlay">
                      <div className="portfolio-info">
                        <h2 className="wow fadeInUp"></h2>
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="portfolio-item wow fadeInUp">
                  <a href="img/portfolio/6.jpg" className="portfolio-popup">
                    <img src="img/portfolio/6.jpg" alt="" />
                    <div className="portfolio-overlay">
                      <div className="portfolio-info">
                        <h2 className="wow fadeInUp"></h2>
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="portfolio-item wow fadeInUp">
                  <a href="img/portfolio/7.jpg" className="portfolio-popup">
                    <img src="img/portfolio/7.jpg" alt="" />
                    <div className="portfolio-overlay">
                      <div className="portfolio-info">
                        <h2 className="wow fadeInUp"></h2>
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-3 col-md-4">
                <div className="portfolio-item wow fadeInUp">
                  <a href="img/portfolio/8.jpg" className="portfolio-popup">
                    <img src="img/portfolio/8.jpg" alt="" />
                    <div className="portfolio-overlay">
                      <div className="portfolio-info">
                        <h2 className="wow fadeInUp"></h2>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="call-to-action" className="wow fadeInUp sec-padding">
          <div className="container">
            <div className="row">
              <div className="col-lg-9 text-center text-lg-left">
                <h3 className="cta-title">Get Our Service</h3>
                <p className="cta-text">
                  N - Squad offers in-home beauty services by the top beauty
                  professionals. -- Let’s make amazing things happen!
                </p>
              </div>
              <div className="col-lg-3 cta-btn-container text-center">
                <a className="cta-btn align-middle" href="#booking">
                  BOOK NOW!
                </a>
              </div>
            </div>
          </div>
        </section>

        <section id="team" className="wow fadeInUp sec-padding">
          <div className="container">
            <div className="section-header">
              <h2>Our Team</h2>
              <p>Find a stylist who matches your style and budget.</p>
              <p>
                Beauty shouldn’t come at a huge cost. Our competitive pricing
                gives salon walk-ins a run for their money. Choose an artist
                from our four levels
              </p>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-6">
                <div className="member">
                  <div className="pic">
                    <img src="img/team1.jpg" alt="" />
                  </div>
                  <div className="details">
                    <h4>James Smith</h4>
                    <span>BGS</span>
                    <p>
                      A stylist with a year of experience and/orbeauty school
                      graduate
                    </p>
                    <div className="social">
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-6">
                <div className="member">
                  <div className="pic">
                    <img src="img/team2.jpg" alt="" />
                  </div>
                  <div className="details">
                    <h4>Michell Kellon</h4>
                    <span>JRS</span>
                    <p>Any stylist with 2 to 5 year of experience</p>
                    <div className="social">
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-6">
                <div className="member">
                  <div className="pic">
                    <img src="img/team3.jpg" alt="" />
                  </div>
                  <div className="details">
                    <h4>French Lincon</h4>
                    <span>VIP</span>
                    <p>
                      A sought-after stylist that has 5 years or more of
                      experience
                    </p>
                    <div className="social">
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-6">
                <div className="member">
                  <div className="pic">
                    <img src="img/team4.jpg" alt="" />
                  </div>
                  <div className="details">
                    <h4>Amanda Jepson</h4>
                    <span>VVIP</span>
                    <p>
                      Hand selected celebrity standard stylists. Exclusively
                      available for you.
                    </p>
                    <div className="social">
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                      <a href="">
                        <i className=""></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="contact" className="wow fadeInUp sec-padding">
          <div className="container">
            <div className="section-header">
              <h2>Contact Us</h2>
              <p>Hello!!!... What can we help you with?</p>
            </div>

            <div className="row contact-info">
              <div className="col-lg-5">
                <div className="contact-address">
                  <i className="ion-ios-location-outline"></i>
                  <h3>Address</h3>
                  <address>
                    L16 B2 Ipil St. Veraville Homes 2, Parañaque
                  </address>
                </div>
                <div className="contact-phone">
                  <i className="ion-ios-telephone-outline"></i>
                  <h3>Phone Number</h3>
                  <p>
                    <a href="tel:+155895548855">+639 1796 542 57</a>
                  </p>
                </div>
                <div className="contact-email">
                  <i className="ion-ios-email-outline"></i>
                  <h3>Email</h3>
                  <p>
                    <a href="mailto:info@example.com">arnelarniego@gmail.com</a>
                  </p>
                </div>
              </div>
              <div className="col-lg-7">
                <div className="container">
                  <div className="form">
                    <form
                      name="sentMessage"
                      className="well"
                      id="contactForm"
                      noValidate
                    >
                      <div className="control-group">
                        <div className="form-group">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Full Name"
                            id="name"
                            required
                            data-validation-required-message="Please enter your name"
                          />
                          <p className="help-block"></p>
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="controls">
                          <input
                            type="email"
                            className="form-control"
                            placeholder="Email"
                            id="email"
                            required
                            data-validation-required-message="Please enter your email"
                          />
                        </div>
                      </div>

                      <div className="form-group">
                        <div className="controls">
                          <textarea
                            rows="10"
                            cols="100"
                            className="form-control"
                            placeholder="Message"
                            id="message"
                            required
                            data-validation-required-message="Please enter your message"
                            minLength="5"
                            data-validation-minlength-message="Min 5 characters"
                            maxLength="999"
                            style={{ resize: "none" }}
                          ></textarea>
                        </div>
                      </div>
                      <div id="success"> </div>
                      <button
                        type="submit"
                        className="btn btn-primary pull-right"
                      >
                        Send
                      </button>
                      <br />
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <footer id="footer">
        <div className="container">
          <div className="copyright">
            &copy; Copyright <strong>Nhel</strong>. All Rights Reserved 2020
          </div>
          <div className="credits">
            {" "}
            <a href="">Nhel</a>
          </div>
        </div>
      </footer>
      <a href="#" className="back-to-top">
        <i className="fa fa-chevron-up"></i>
      </a>
    </>
  );
};

export default Home;
