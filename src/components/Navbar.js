import React, { useContext } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../context/auth/authContext";

const Navbar = () => {
  const authContext = useContext(AuthContext);
  const { isAuthenticated, logoutUser, user } = authContext;

  return (
    <>
      <section id="topbar" className="d-none d-lg-block">
        <div className="container clearfix">
          <div className="contact-info float-left">
            <i className="fa fa-envelope-o"></i>{" "}
            <a href="mailto:contact@example.com">arnelarniego@gmail.com</a>
            <i className="fa fa-phone"></i> +639 1796 542 57
          </div>
          <div className="social-links float-right">
            <a href="#" className="twitter">
              <i className="fa fa-twitter"></i>
            </a>
            <a href="#" className="facebook">
              <i className="fa fa-facebook"></i>
            </a>
            <a href="#" className="google-plus">
              <i className="fa fa-google-plus"></i>
            </a>
            <a href="#" className="linkedin">
              <i className="fa fa-linkedin"></i>
            </a>
            <a href="#" className="instagram">
              <i className="fa fa-instagram"></i>
            </a>
          </div>
        </div>
      </section>
      <header id="header">
        <div className="container">
          <div id="logo" className="pull-left">
            <h1>
              <a href="/#body" className="scrollto">
                <span>N</span> - <span>S</span>quad
              </a>
            </h1>
          </div>

          <nav id="nav-menu-container">
            <ul className="nav-menu">
              <li className="menu-active">
                <a href="/#body">Home</a>
              </li>
              <li>
                <a href="/#services">Services</a>
              </li>
              <li>
                <a href="/#about">About Us</a>
              </li>
              <li>
                <a href="/#team">Our Team</a>
              </li>
              <li>
                <a href="/#portfolio">Gallery</a>
              </li>
              <li>
                <a href="/#contact">Contact</a>
              </li>
              <li>
                <a href="/#booking">Appointment</a>
              </li>
              <li className="menu-has-children">
                <a href="#account">Account</a>
                <ul>
                  {isAuthenticated ? (
                    <>
                      {user.email === "test321@gmail.com" && (
                        <li>
                          <Link to="/admin">Admin</Link>
                        </li>
                      )}
                      <li>
                        <Link to="/appointments">Appoinments</Link>
                      </li>
                      <li>
                        <a href="#logout" onClick={logoutUser}>
                          Logout
                        </a>
                      </li>
                    </>
                  ) : (
                    <>
                      <li>
                        <Link to="/login">Login</Link>
                      </li>
                      <li>
                        <Link to="/register">Register</Link>
                      </li>
                    </>
                  )}
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    </>
  );
};

export default Navbar;
